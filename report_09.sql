SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>OLVOYAGE - RAPPORT GROUPE B1B 07</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #eff0f4; text-align: left} --> -
</STYLE>" -
BODY "TEXT='#394b5d'" -
TABLE "WIDTH='90%' BORDER='5'" 
TH "color='#FACB56'"
SPOOL report_01.htm;
SET PAGESIZE 40;
			SELECT train.Train_id "Numero", 
			station.city ||' - '|| station2.city "Train" ,
			TRUNC(distance/((arrival_time - departure_time)*(60*24))*60, 0)||' km/h' "Vitesse Moyenne"	
            FROM t_Train train	
            JOIN t_station station	
            ON train.departure_station_id=station.station_id
            JOIN t_station station
            ON train.arrival_station_id=station.station_id	
            ORDER BY train_id;	
SPOOL OFF;