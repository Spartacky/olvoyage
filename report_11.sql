SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>OLVOYAGE - RAPPORT GROUPE B1B 07</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #eff0f4; text-align: left} --> -
</STYLE>" -
BODY "TEXT='#394b5d'" -
TABLE "WIDTH='90%' BORDER='5'" 
TH "color='#FACB56'"
SPOOL report_01.htm;
SET PAGESIZE 40;
				SELECT train.train_id ||' '||'('|| station.city ||'-'|| station2.city||')' nom_train , train.price prix, p.pass_name nom_abonnement,
                CASE
                WHEN TO_CHAR(train.arrival_time, 'Day') IN ('Samedi', 'Dimanche')	
                THEN (train.price*(1-(NVL(p.discount_we_pct, 0)/100)))
                WHEN TO_CHAR(train. arrival_time, 'Day') = 'Vendredi' AND TO_CHAR(train. arrival_time, 'HH24') > 18
                THEN (train.price*(1-(NVL(p.discount_we_pct, 0)/100)))	
                ELSE (train.price*(1-(NVL(p.discount_pct, 0)/100)))
                END "PRIX"
                FROM t_train train
				JOIN t_station station
                ON train.departure_station_id=station.station_id
				JOIN t_station station2
                ON train.arrival_station_id=station2.station_id
                CROSS JOIN t_pass p
                WHERE station.city = 'Paris';				
SPOOL OFF;