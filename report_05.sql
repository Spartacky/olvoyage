SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>OLVOYAGE - RAPPORT GROUPE B1B 07</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #eff0f4; text-align: left} --> -
</STYLE>" -
BODY "TEXT='#394b5d'" -
TABLE "WIDTH='90%' BORDER='5'" 
TH "color='#FACB56'"
SPOOL report_01.htm;
SET PAGESIZE 40;
				SELECT
				train.train_id||' '||
				station.city||' '||
				'('||TO_CHAR(departure_time, 'DD/MM/YY HH24:MI')||')' || ' - '||
				station2.city||' '||
				'('||TO_CHAR(arrival_time, 'DD/MM/YY HH24:MI')||')'
				"trajet",
				price "prix",
				train.distance "distance"
				FROM t_train train
				JOIN t_station station
				ON train.departure_station_id=station.station_id
				JOIN t_station station2
				ON train.arrival_station_id=station2.station_id
				ORDER BY train_id;
SPOOL OFF;