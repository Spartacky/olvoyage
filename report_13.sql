SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>OLVOYAGE - RAPPORT GROUPE B1B 07</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #eff0f4; text-align: left} --> -
</STYLE>" -
BODY "TEXT='#394b5d'" -
TABLE "WIDTH='90%' BORDER='5'" 
TH "color='#FACB56'"
SPOOL report_01.htm;
SET PAGESIZE 40;
				  SELECT ti.ticket_id num_billet, UPPER(c.last_name)||' '|| c.first_name "Client-25ans", depart.city ||' '||'('||
                  TO_CHAR(t.departure_time, 'DD/MM/YYY HH24:MI')||')' || ' - ' || arrive.city ||' '|| '('||
                  TO_CHAR(t.arrival_time,'DD/MM/YYYY HH24:MI')||')' "Trajet"
                  FROM t_train t
                  JOIN t_station depart
                  ON depart.station_id = t.departure_station_id
                  JOIN t_station arrive
                  ON arrive.station_id = t.arrival_station_id
                  JOIN t_wagon_train w
                  ON t.train_id = w.train_id
                  JOIN t_wagon wa
                  ON w.wagon_id = wa.wagon_id
                  JOIN t_ticket ti
                  ON w.wag_tr_id = ti.wag_tr_id
                  JOIN t_reservation r
                  ON ti.reservation_id = r.reservation_id
                  JOIN t_customer c
                  ON r.buyer_id = c.customer_id
                  WHERE MONTHS_BETWEEN(SYSDATE, birth_date) < 300
                  AND wa.class_type = 1
                  AND departure_time BETWEEN '15/01/2017' AND '25/01/2017'
                  AND r.creation_date <= t.departure_time-20
                  ORDER BY r.creation_date;
SPOOL OFF;