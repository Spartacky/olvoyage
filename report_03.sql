SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>OLVOYAGE - RAPPORT GROUPE B1B 07</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #eff0f4; text-align: left} --> -
</STYLE>" -
BODY "TEXT='#394b5d'" -
TABLE "WIDTH='90%' BORDER='5'" 
TH "color='#FACB56'"
SPOOL report_01.htm;
SET PAGESIZE 40;
				SELECT r.creation_date, UPPER(e.last_name) ||' '|| e.first_name "employe", UPPER(c.last_name) ||' '|| c.first_name "acheteur"
                FROM t_employee e JOIN t_reservation r ON r.employee_id = e.employee_id 
				JOIN t_ticket t ON r.reservation_id = t.reservation_id
				JOIN t_customer c ON (c.customer_id = t.customer_id)
                HAVING MIN(r.creation_date) IN (SELECT MIN(creation_date)FROM t_reservation)
				GROUP BY r.creation_date, e.last_name, e.first_name, c.last_name, c.first_name;
SPOOL OFF;