SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>OLVOYAGE - RAPPORT GROUPE B1B 07</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #eff0f4; text-align: left} --> -
</STYLE>" -
BODY "TEXT='#394b5d'" -
TABLE "WIDTH='90%' BORDER='5'" 
TH "color='#FACB56'"
SPOOL report_01.htm;
SET PAGESIZE 40;
				SELECT *
				FROM (SELECT train.train_id|| ' ' ||station.city||' - '||station2.city "trajet", COUNT(reservation_id) nombre_resa
                FROM t_train train
                JOIN t_wagon_train wagon
                ON train.train_id=wagon.train_id
                JOIN t_ticket ticket
                ON wagon.wag_tr_id=ticket.wag_tr_id
                JOIN t_reservation reservation
                ON ticket.reservation_id=reservation.reservation_id
                JOIN t_station station
                ON train.departure_station_id=station.station_id
                JOIN t_station station2
                ON train.arrival_station_id=station2.station_id
                GROUP BY train.train_id,ts.city,station2.city
                ORDER BY COUNT(reservation_id) DESK)
                WHERE rownum <=5;
SPOOL OFF;