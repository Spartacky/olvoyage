SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>OLVOYAGE - RAPPORT GROUPE B1B 07</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #eff0f4; text-align: left} --> -
</STYLE>" -
BODY "TEXT='#394b5d'" -
TABLE "WIDTH='90%' BORDER='5'" 
TH "color='#FACB56'"
SPOOL report_01.htm;
SET PAGESIZE 40;
				SELECT UNIQUE(UPPER(c.last_name)||' '||c.first_name) AS resa_autre_personne
                FROM t_reservation r 
                JOIN t_ticket t ON(t.reservation_id = r.reservation_id)
                JOIN t_customer c ON (c.customer_id = t.customer_id)
                WHERE r.buyer_id != c.customer_id
                ORDER BY resa_autre_personne;
SPOOL OFF;