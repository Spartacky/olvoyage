SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>OLVOYAGE - RAPPORT GROUPE B1B 07</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #eff0f4; text-align: left} --> -
</STYLE>" -
BODY "TEXT='#394b5d'" -
TABLE "WIDTH='90%' BORDER='5'" 
TH "color='#FACB56'"
SPOOL report_01.htm;
SET PAGESIZE 40;
	SELECT distinct count(r.reservation_id) "Resa", LPAD(upper(e.last_name)||' '||te.first_name, LENGTH(te.last_name||' '||te.first_name)+(LEVEL*2)-2, '_') "Employe", e.employee_id num_employe
	FROM t_employee e
	LEFT OUTER JOIN  t_reservation r
	ON e.employee_id=r.employee_id
	START WITH manager_id = (select employee_id from t_employee where manager_id IS NULL)
	CONNECT BY PRIOR r.employee_id=manager_id
	GROUP BY LPAD(upper(e.last_name)||' '||e.first_name, LENGTH(e.last_name||' '||te.first_name)+(LEVEL*2)-2, '_'), te.employee_id
	ORDER by e.employee_id;
SPOOL OFF;