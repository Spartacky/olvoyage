SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>OLVOYAGE - RAPPORT GROUPE B1B 07</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #eff0f4; text-align: left} --> -
</STYLE>" -
BODY "TEXT='#394b5d'" -
TABLE "WIDTH='90%' BORDER='5'" 
TH "color='#FACB56'"
SPOOL report_01.htm;
SET PAGESIZE 40;
				SELECT UPPER(c.last_name)||' '||c.first_name "Abonné",p.pass_name "abonnement",
				CASE
				WHEN add_months(c.pass_date,12) IS NULL THEN 'Aucun'
				WHEN add_months(c.pass_date,12) < to_date('30-Novembre-2016') then 'Périmé !'
				END "titre périmé !"
				FROM t_customer c
				JOIN t_pass p
				ON (c.pass_id = p.pass_id )
				UNION ALL
				SELECT UPPER(last_name)||' '||first_name "Abonné",to_char(null) "abonnement",
				CASE
				WHEN add_months(pass_date,12) IS NULL THEN 'Aucun'
				WHEN add_months(pass_date,12) < to_date('30-Novembre-2016') then 'Périmé !'
				END "titre_périmé !"
				FROM t_customer
				WHERE pass_date IS NULL
				ORDER BY "Abonné";
SPOOL OFF;