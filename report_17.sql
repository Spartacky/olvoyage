define resa =''
define Moyen_de_paiement=''
undefine resa
undefine Moyen_de_paiement
UPDATE T_RESERVATION
SET buy_method = '&Moyen_de_paiement', price = 
(SELECT CASE
	WHEN TO_CHAR(t.arrival_time, 'Day') IN ('Samedi', 'Dimanche')
	THEN (t.price*(1-(NVL(p.discount_we_pct, 0)/100)))*(1+(NVL(wa.class_pct, 0)/100))
	WHEN TO_CHAR(t. arrival_time, 'Day') = 'Vendredi' AND TO_CHAR(t. arrival_time, 'HH24') > 18
	THEN (t.price*(1-(NVL(p.discount_we_pct, 0)/100)))*(1+(NVL(wa.class_pct, 0)/100))
	ELSE
	(t.price*(1-(NVL(p.discount_pct, 0)/100)))*(1+(NVL(wa.class_pct, 0)/100))
	END
	FROM T_RESERVATION r
	JOIN T_CUSTOMER c
	ON c.customer_id = r.buyer_id
	LEFT OUTER JOIN T_PASS p
	USING (pass_id)
	JOIN T_TICKET ti
	SING (reservation_id)
	JOIN T_WAGON_TRAIN
	USING (wag_tr_id)
	JOIN T_TRAIN t
	USING (train_id)
	JOIN T_WAGON wa
	USING (wagon_id)
	WHERE reservation_id = &&resa)
WHERE reservation_id = &resa;