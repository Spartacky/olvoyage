SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>OLVOYAGE - RAPPORT GROUPE B1B 07</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #eff0f4; text-align: left} --> -
</STYLE>" -
BODY "TEXT='#394b5d'" -
TABLE "WIDTH='90%' BORDER='5'" 
TH "color='#FACB56'"
SPOOL report_01.htm;
SET PAGESIZE 40;
						SELECT COUNT(reservation.reservation_id) Resa_Reduc_Senior_janvier_2017
						FROM t_reservation reservation
						JOIN t_ticket ticket
						ON tr.reservation_id=ticket.reservation_id
						JOIN t_wagon_train wagon
						ON ticket.wag_tr_id=wagon.wag_tr_id
						JOIN t_train train
						ON wagon.train_id=train.train_id
						JOIN t_customer c
						ON ticket.customer_id=c.customer_id
						JOIN t_pass pass
						ON c.pass_id=pass.pass_id
						WHERE discount_pct=40
						AND reservation.buy_method IS NOT NULL
						AND train.departure_time LIKE '__/%01/17%';
SPOOL OFF;