SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>OLVOYAGE - RAPPORT GROUPE B1B 07</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #eff0f4; text-align: left} --> -
</STYLE>" -
BODY "TEXT='#394b5d'" -
TABLE "WIDTH='90%' BORDER='5'" 
TH "color='#FACB56'"
SPOOL report_01.htm;
SET PAGESIZE 40;
				SELECT UPPER(e.last_name)|| ' '|| e.first_name  meilleur_employe
                FROM t_employee e
                JOIN t_reservation r
                ON (e.employee_id = r.employee_id)
                GROUP BY e.first_name,e.last_name
                HAVING COUNT(r.reservation_id) = ANY (
				SELECT MAX(COUNT(r.reservation_id))
                FROM t_employee e
                JOIN t_reservation r
                ON (e.employee_id = r.employee_id )
                GROUP BY e.last_name,e.first_name);
SPOOL OFF;