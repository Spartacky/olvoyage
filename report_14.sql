SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>OLVOYAGE - RAPPORT GROUPE B1B 07</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #eff0f4; text-align: left} --> -
</STYLE>" -
BODY "TEXT='#394b5d'" -
TABLE "WIDTH='90%' BORDER='5'" 
TH "color='#FACB56'"
SPOOL report_01.htm;
SET PAGESIZE 40;				
					SELECT train.train_id || ' - '|| station.city || ' - ' || station2.city "Train", sum(tw.nb_seat)-count(ttk.ticket_id) "Nombre places"
					FROM t_train train
					JOIN t_station station
					ON train.departure_station_id=station.station_id
					JOIN t_station station2
					ON train.arrival_station_id=station2.station_id
					JOIN t_wagon_train wagontrain
					ON train.train_id=wagontrain.train_id
					JOIN t_wagon wagon
					ON wagontrain.wagon_id=wagon.wagon_id
					JOIN t_ticket ticket
					ON wagontrain.wag_tr_id=ticket.wag_tr_id
					WHERE wagontrain.train_id in (select train.train_id from t_train)
					AND train.distance > 300
					AND train.departure_time LIKE '22/01/17'
					GROUP BY train.train_id, station.city, station2.city
					ORDER BY train.train_id;			
SPOOL OFF;