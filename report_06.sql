SET MARKUP HTML ON SPOOL ON -
HEAD "<TITLE>OLVOYAGE - RAPPORT GROUPE B1B 07</TITLE> -
<STYLE type='text/css'> -
<!-- BODY {background: #eff0f4; text-align: left} --> -
</STYLE>" -
BODY "TEXT='#394b5d'" -
TABLE "WIDTH='90%' BORDER='5'" 
TH "color='#FACB56'"
SPOOL report_01.htm;
SET PAGESIZE 40;
				 SELECT
                (SELECT COUNT(employee_id) FROM t_employee) "Employe",
                (SELECT COUNT(customer_id) FROM t_customer) "Client",
                (SELECT ROUND(((SUM(NVL2(pass_id,1,0 )))/COUNT(customer_id))*100,2) FROM t_customer)  "% ABONNES",
               (SELECT COUNT(train_id) FROM t_train) "Train",
               (SELECT COUNT(station_id) FROM t_station) "Station",
               (SELECT COUNT(reservation_id) FROM t_reservation) "Reservation",
               (SELECT COUNT(ticket_id) FROM t_ticket) "Ticket"
                FROM dual;
SPOOL OFF;